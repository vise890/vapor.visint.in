+++
title = "🎩 About"
weight = 3
+++

## Hi! I'm Martino

I'll get around writing something here at some point.

In the meantime, here are some links:

- [✉️ `martino@visint.in`](mailto:martino@visint.in).
- [🦊 GitLab](https://gitlab.com/vise890/)
- [📸 photos](https://photos.visint.in)
- [📜 resume](https://vise890.gitlab.io/resume/resume.html)

> **`NOTE:`** this [site's source is on Gitlab](https://gitlab.com/vise890/vapor).
