+++
title = "edn++"
+++

like [edn](https://github.com/edn-format/edn), but with a few more default
`data-readers`.

```edn
{uuid #'clojure.uuid/default-uuid-reader,
 inst #'clojure.instant/read-instant-date
 instant #'clojure.instant/read-instant-date
 local-time null
 local-date-time null
 duration null
 ,,,,}
```
