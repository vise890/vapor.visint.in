+++
title = "🍥 Dataflow"
+++

## ....where it all comes together (sort-of)

This _puff_ tries to unravel the general problem of ingesting and making
sense of data from the "outside" (and sending it back). In my experience, the
process goes roughly like this:

> parse the data into some sort of typed object and validate it;
> else throw an error or relax the schema.

In my opinion, trying to do it in smaller steps, provides some value at
each stage.

Essentially the dataflow looks like this:

```txt
raw₁ -> untyped₁  -> typed -> validated -> (untyped₂) -> raw₂
```

1. you [_parse_](#1parse) the `raw` data that comes in, transforming it into some sort of `untyped` structure;
   - think of it as JSON with all values being `String`, or a CSV parsed into a vector of vectors of `Strings`.
   - `untyped` structured data can often already be loaded into a DB,
     queried, and analysed, and thus inform and the later steps.
1. you [_type_](#2type) the `untyped` values by trying to coerce them into a `typed` structure;
   - this and the previous step are often done together, but, as described
     above, there is value in being able to query and fiddle with `untyped` data;
   - similarly to the above you can ingest `typed` data and analyse it,
     before it is `validated`
1. you [_validate_](#3validate) the `typed` data, making it `validated`
   - note that there could be different ways of validating it; especially at
     the beginning, when a coherent, universal set of validation rules hasn't
     emerged yet. For example one team might not care if a field is positive or
     negative, while another will.

## 1. Parsing / Deserialising {#1parse}

`raw` -> `untyped`

Data coming from the outside world comes in a number of `raw` formats. This
is the process of getting it into EDN with mostly `String` values (some may
be `Numeric`, e.g., when parsing binary).

| Source Format  | How to parse it into EDN          |
| -------------- | --------------------------------- |
| binary         | [binparse](@/1_rings/binparse.md) |
| csv/tsv/fixed  | [vsc](@/1_rings/vsc.md)           |
| EDIFACT et al. | [parseq](@/1_rings/parseq.md)     |
| json           | whatever                          |
| avro           | whatever                          |
| ...            | whatever                          |

## 2. Typing {#2type}

`untyped` -> `typed`

A.k.a. coercing untyped data (i.e., `Strings` and possibly numbers) into fully typed data
(e.g., `enum`s, `Instant`s, `LocalDate`s, `Set`s).

The discovery of the coercing functions can be:

- Fully manual:
  - hard to get right on first try;
  - repetitive.
  - tools:
    - [clojure spec coercers](https://clojure.org/guides/spec)
- Computer aided:
  - Faster, more iterative approach;
  - [`typeguess`](@/2_puffs/typeguess.md) + [Trifacta-like](https://www.trifacta.com/) UI, but for nested data;
  - Inference driven:
    - [F# type providers](https://docs.microsoft.com/en-us/dotnet/fsharp/tutorials/type-providers/);
    - [`malli` inferred schemas](https://github.com/metosin/malli#user-content-inferring-schemas);
    - [Clojure `spec-provider`](https://github.com/stathissideris/spec-provider).

## 3. (Logical) Validation {#3validate}

`typed` -> `validated`

- Fully manual
  - custom code
  - [malli](https://clojure.org/guides/spec)
  - [clojure spec coercers](https://clojure.org/guides/spec)
  - [cue](https://cuelang.org/)
  - [javax.validation](https://www.baeldung.com/javax-validation)
- Computer aided:
  - could infer some simple rules (e.g. +ve/-ve/enum) like
    [`spec-provider`](https://github.com/stathissideris/spec-provider) does?
- `TODO:` must be able to provide **human-readable** error messages

## ⚗️ Distilled Data

where the many `validated` data are sent around, stored, queried, analysed,
machine-learned, put on the blockchain, sliced and reassembled to solve the
Business Problem™

- Kafka Streams ([`onion`](@/2_puffs/onion.md)) to filter, transform data;
- Kafka Connect ([`onion`](@/2_puffs/onion.md) again?) to ingest it in specific DBs where can be analysed and spat back out;
- Ad-hoc services to do ad-hoc stuff.

## 4. (Logical) Validation (again, possibly?)

`typed` -> `typed`

## 5. Untyping

`typed` -> `untyped`

## 6. Serializing

`untyped` -> `raw`

`raw` data is ready to be sent back to the world in the format it came in.

## Testing the pipeline

We've seen the dataflow pipeline goes something like this:

```txt
raw₁ -> untyped₁  -> typed -> validated -> (untyped₂) -> raw₂
```

You should be able to round-drip some sample data and:

```python
assert(raw₁ == raw₂)
```

If that is not possible, perhaps you can test that:

```python
assert(untyped₁ == untyped₂)
```

## Misc

You will generate lots of tables/kafka topics. In my experience including the
stage ({`raw` `untyped`, `typed`, `validated`, `validated.xyz`} and
versioning them to be able to evolve (at some point
you may be publishing to multiple `_v`s of a topic to give people time to upgrade).
E.g.,

- `data_D0918_untyped_v1`
- `data_D0918_typed_v3`

You may then have a similar naming schema for your schemas/tables in your
DBs.

Something like [topicmop](@/2_puffs/topicmop.md) should be built to help.

## 🎨 Prior art

- [Trifacta](https://www.trifacta.com/)
- [Univocity](https://www.univocity.com/pages/about_univocity)
- [malli](https://github.com/metosin/malli) (Data driven schemas for clojure/cljs)
