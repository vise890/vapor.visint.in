+++
title = "☁️ puffs"
weight = 2
+++

These are _puffs_; they are more like nebulous swirls of ideas than somewhat
fleshed-out concepts.

As they get more refined, they graduate to [💍 _rings_](@/1_rings/_index.md).
