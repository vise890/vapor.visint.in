+++
title = "👀 fokus"
+++

In KDE, when using a laptop and a big screen, and there is one maximized
window per monitor, use laptop webcam to shift focus to the window in the
monitor you are looking at.
