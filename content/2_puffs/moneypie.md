+++
title = "💰 moneypie"
+++

Takes the transactions `.csv` from your bank and a toml config as argument.
Outputs a pie(?) chart of expenses by category

## Config

In which the categories are set up, and the format is described

```toml
# for a two-column statement where all amounts are +ve
# - blank cells are skipped
# - assume all amounts are positive
[xlsx]
sheet = "ListaMovimenti"
skip_rows = 12
description_column = 4 # "E"
amount_out_column = 2 # "C"
amount_in_column =  3 # "D"

# for single column statement
# - blank cells produce an error
# - assumes money out is -ve and money in is +ve
# [xls.bank2]
# sheet = "Movimenti CC"
# skip_rows = 9
# description_column = 4 # "E"
# amount_column = 5 # "F"

[numbers]
separator_thousands = "." # default ","
separator_decimal = ","   # default "."


[categories.ignore]
ignore = true
description_regex = [
  "BONIFICO.*IN ENTRATA.*DA MARTINO VISINTIN",
  "VOSTRO BONIFICO.*BENEFICIARIO: VISINTIN"
]

[csv.description.categories.groceries]
description_regex = [
  "SUPERMERCATO",
  "EUROSPAR",
]

[categories.games]
description_regex = [
  "steam",
]

[categories.online_shopping]
description_regex = [
  "AMZN Mktp",
  "Amazon Prime",
  "AMZ*",
  "ALIEXPRESS",
  "PAYPAL",
]

[categories.salary]
description_regex = [
  "BONIFICO DEL.+PER STIPENDIO",
]

[categories.banking_fees]
description_regex = [
  "CANONE CONTO",
  "IMPOSTA BOLLO",
  "SPESE PER BONIFICO",
  "ADDEBITO SPESE RAPPORTO IN TITOLI",
  "COMMISSIONI E SPESE SU OPERAZIONI CON L'ESTERO"
]

[categories.savings]
description_regex = [
  "SOTTOSCRIZIONE TITOLI E/O FONDI COMUNI",
]

[categories.medical]
description_regex = [
  "FARMACIA",
  "DENTALTEAM",
]

[categories.social]
description_regex = [
  "ANTICA CONTEA BIRRIFIC",
]
```

## Output: CSV

```csv
+2000.00,salary
-8.80,social
-25.37,social
-200.00,savings
-75.42,medical
```

## Output: Chart

A piechart or [an alternative](https://www.tableau.com/about/blog/2019/1/5-unusual-alternatives-pie-charts-100071).

## 🧱 Building aids

- [`piechart` crate](https://lib.rs/crates/piechart/)
- [`vega_4_lite` crate](https://lib.rs/crates/vega_lite_4)
