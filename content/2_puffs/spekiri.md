+++
title = "🧩 Spekiri"
+++

Something like clj spec, but described in **data**:

> Looks like [`malli`](https://github.com/metosin/malli) already meets most
> of these criteria, except the `cue`-like additivity.

- similar `cue`, but defined in EDN + AERO
- rdf-style `definitions` like in `clj.spec`, (i.e., namespaced keywords)
  rather than structure based like in `cue`
- `definitions` can then be assembled into `constructs` like `s/keys` in
  clojure-spec
- both `definitions` and `constructs` can be combined like in `cue`.
  (They're both Monoids I guess, but except that order doesn't matter)
- a `spec` is a map containing
  - just the rdf-style defs? (i.e. no aggregates?)
  - both definitions and constructs?

## coercers + inference

`untyped` -> `typed`

> [See Typing in Dataflow](@/2_puffs/dataflow.md#2type)

- `coercers` (for each "source" type, only one can exist)
- inferencers (in a lattice, with priority? in a tree? can just have one per node in cuelang-talk))
- generate avro schemas, json schemas, swagger docs, etc from it
- clojure-spec-provider but that outputs spekiri-ready schema + possible multiple ones a la cuelang

## 🎨 Prior art

- [malli](https://github.com/metosin/malli) (Data driven schemas for clojure/cljs)
- [cue](https://cuelang.org/)
- [Clojure spec](https://clojure.org/guides/spec)
