+++
title = "🐃 Emacs++"
+++

Emacs, but with a modern lisp as its base.

By "modern lisp" I mean something that can leverage bigger ecosystems, has a
decent dependency management story, good documentation, etc. (Kind of what
[`jaunt`](@/2_puffs/jaunt.md) is all about).

## Features

- be the best lisp-machine available
- deployable as a runtime / runtime-image with state?

## 🎨 Prior art

- smalltalk images
