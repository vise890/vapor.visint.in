+++
title = "🧹 topicmop"
+++

Helps you cleaning up unused Kafka topics

- every service exposes a `/topics` endpoint that lists input and output topics
- input/output topics are read from [`onion` configs](@/2_puffs/onion.md)
- a K8s job gets run every 12h
  - scrapes `/output_topics` and `onion` configs
  - and reports topics that no service/stream is using anymore
- Also
  - keep up with `_consumers` and report about topics with no consumer groups

## 4 DBs too

Same thing for db tables that are ingested through Kafka Connect or similar
