+++
title = "🪦 undertaker"
+++

- you place [tombstones](https://github.com/scheb/tombstone) around the codebase, implemented like a lib
- you send us your tombstones (through CI / git hook)
- we alert you if a tombstone was touched
- we notify you when it is safe to remove tombstone-protected code block
- you give us ca$h, like 2EUR/month
