+++
title = "📋 kanbo"
+++

A kanban board you can check into your VCS.

## 🏛 Columns

Columns are directory in `board`, e.g.:

- `board/backlog/`
- `board/technical_debt/`
- `board/selected_for_dev/`
- `board/doing/`
- `board/reviewing/`
- `board/done/`

## 🃏 Cards

Cards are `.md` files in their respective column/directory. You move cards
with `git mv`. Cards have a `toml` front-matter that contains things such as position. Comments come in a `## Comments` section as blockquotes. Git blame can be used to display who said what when (git-lens probably would already do this).

Card templates are `.md` files in `board/TEMPLATES` or cards in a Column, but with `template = true` in their front-matter.

## what it needs

- cli
- web ui for drag/drop

## 🎨 Prior art

- [taskell](https://taskell.app/)
