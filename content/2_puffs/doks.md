+++
title = "📚 doks"
+++

KRunner plugin to search through dash/zeal docs and open them in a big fat
(Zeal?) window.

## Prior art

- zeal
- dash + alfred
