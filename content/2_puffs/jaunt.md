+++
title = "☯️ jaunt"
+++

A leaner, **community-based** clojure-like language, that leverages existing
ecosystems even more.

## 🧑‍🤝‍🧑 Community, community, community

- awesome community (see rust, elixir)
- awesome tooling (see rust, elixir, kotlin, go?)
- awesome docs (see **rust**, elixir?)

## 📦 Leverage existing stuff

- implemented
- target kotlin IR and get js/jvm/native implementations for free rather than
  having separate compilers?;
- off the shelf parser library for 1) Easier implementation; 2) Better parse errors
- best-in-class data structures, taken from existing libs
  - mutable from jvm/kotlin
  - immutable? from kotlin/guava
  - persistent from kotlin?

## ✨ Minimal and clean implementation

- core should be as small and as simple as possible
  - no extra namespaces (reflection/tap/reducers/..)
  - no extra functionality that could be a lib (stm, agents)
  - no interop in core.xyz
- support for imperative programming / mutability in the small, but
  discouraged and make it look uglier (e.g. janet)
- can participate in protocols (no special interfaces)

## 💎 That warm, fuzzy, human Ruby feel

- have some tasteful aliases?
  - `count` `length` `size`
  - `some?` `any?`
  - `not-any?`
  - `every?` `all?`
  - `not-every?` `not-all?`
- have some smaller functions that declare intent more explicitly
  - `some` return first truthy pred(val)
  - vs `some?`is there is any val that satisfies the pred?
    - it says: the element value is not gonna be used
  - i know it's not lispy but to me it's clearer

## 🎨 Prior art

- [Crafting interpreters](https://craftinginterpreters.com/)
- [Janet](https://janet-lang.org/)
- [Small Clojure Interpreter](https://github.com/borkdude/sci)
- [joker](https://github.com/candid82/joker/blob/master/README.md)

> **`TODO:`** find that clj-compatible set of experiments that were made by
> some guy at some point to simplify namespaces, fix math, make IMeta and stuff
> into protocols, etc.
