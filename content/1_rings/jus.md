+++
title = "🍹 jus"
+++

> **jus** (ʒuː; French ʒy)
>
> gravy, broth, sauce — wiktionary.org

`jus` is a cli tool for extracting, viewing, creating and modifying archives
of all kinds.

Supported formats:

- [ ] 7z
- [ ] ace
- [ ] arj
- [ ] bzip2
- [ ] cpio
- [ ] gzip
- [ ] lha
- [ ] lzh
- [ ] lzma
- [ ] lzma
- [ ] lzo
- [ ] rar
- [ ] rzip
- [ ] tar
- [ ] xz
- [ ] zip
- [ ] zpaq

## Installation

```sh
cargo install jus
```

## Usage

```sh
# Archive a file or directory:
jus a archive.7z path/to/file_or_directory

# Jus infers the archive type from the extensions.
jus a archive.zip path/to/file_or_directory
jus a archive.tar.gz path/to/file_or_directory

# Extract files leaving original directory structure intact:
jus x archive.tar.gz

# Extract files to a specified path:
jux x archive.7z path/to/output_directory

# List available archive types:
jus i

# List the contents of an archive:
jus l archive.rar

# Test archives?
# Out to STDOUT?
# Parallel impl?
# Encrypted archives?
```
