+++
title = "🌀 vsc"
+++

`vsc` is a reversible csv, tsv, and fixed-with parser.

I think this can be done by wrapping
[univocity-parsers](https://www.univocity.com/pages/univocity_parsers_tutorial).

```clj
(def contents (slurp "file.csv"))

(def csv (vsc/str->cells contents))

cells
#=> [[1 2 3] ["a" "b" "c"]]

(meta cells)
#=> {:vsc/settings
     {:delimiter \;
       :eol "\n"
       :quote-char \"
       :always-quoted-cols ["date"]
       ,,,}}

(def str
  (vsc/cells->str cells
                  (-> cells meta :vsc/settings)))

(assert (= contents str))
```

## Open questions

- how do we deal with input values that were quoted in the input but didn't
  need to be?
  - can we introduce detection while reading and add a
    `:always-quoted-cols` to the `settings`?
  - what about mixed columns? forget about it?

## 🎨 Prior art

- [data-csv](https://github.com/davidsantiago/clojure-csv) (for inspiration for the interface)
- [univocity-parsers](https://www.univocity.com/pages/univocity_parsers_tutorial)
