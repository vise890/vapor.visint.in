+++
title = "🤖 binparse"
+++

A nicer data-based way to parse binary files.
Could start with kaitai and make it **reversible**, dammit.

## 🎨 Prior art

- OCaml's [bitstring](https://bitstring.software/examples/)
- Pattern matching on binaries in [Elixir](https://elixir-lang.org/getting-started/binaries-strings-and-char-lists.html#bitstrings) or Erlang
- [Kaitai Struct](https://kaitai.io/)
  - [Relevant Kaitai Serialization issue](https://github.com/kaitai-io/kaitai_struct/issues/27)
- [`nom`?](https://github.com/geal/nom)
