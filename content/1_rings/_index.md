+++
title = "💍 rings"
weight = 1
+++

_Rings_ are almost formed concepts that are ready to be built. They're not as
detailed as a full specification, but they should be descriptive enough to
give a general idea on where to start, what the main requirements and
features would be, and so on.

Younger projects that are still not quite fleshed-out reside in the
[☁️ _puffs_](@/2_puffs/_index.md) section.
