+++
title = "🉁 Sets in todo.sql"
+++

In [todo.sql](@/1_rings/tudor/3_todo.sql.md) we have seen how to create simple views with
the Canonical Context (`ccontext`) and the Canonical Project (`cproject`).

Doing this, works most of the time and it behaves like some other todo.txt
clients (e.g. [Simpletask](https://github.com/mpcjanssen/simpletask-android)).

However, if you want to be more precise, you should use the `context` set.

```sql
-- 🚀 most_important tasks
-- Say you want to exclude some contexts from your view.
-- You could try:
select *
from active_tasks
where ccontext not in ('@tv', '@gameboy')
-- ... but it wouldn't be quite right. For example, it would match the todo:
--    '@home @tv watch last episode of south park'
-- ... since '@home' would be the todo's Canonical Context (`ccontext`), and '@home'
-- is not in ('@tv', '@gameboy')
--
-- Instead, the right way to do it in tudor is to use the `contexts` set:
select *
from active_tasks
where not (contexts && ('@tv', '@gameboy'))

-- Sets in tudor work similarly to Arrays in PostgreSQL, but the only operators
-- supported are: @>, <@, and &&.
--
-- See the pg docs for more info:
-- https://www.postgresql.org/docs/9.0/functions-array.html


-- Some other examples:


-- 📮 inbox
select *
from todo_txt
where is_empty(contexts)


-- 👓 selecting from other views
select *
from active_tasks
-- tasks that are either in +proj1 *or* +prj2 (or in both)
where projects && ('+proj1', '+proj2')


-- 🔮 future tasks
select * from todo_txt
where contexts && ('@@blocked', '@@maybe')
  or threshold_date > date('today')


-- 🛑 blocked tasks
select *
from todo_txt
where contexts && ('@@blocked')
  or is_blocked = true
```
