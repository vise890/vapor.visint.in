+++
title = "🖼️ TUI"
weight = 2
+++

The TUI has a two-pane interface, which allows for faster interaction with
your todos with autocompletion, custom collections/views, smart and fast task
input, and fuzzy search.

## ○ Left pane

### 👓 Views

All the views (and their icon, if present) are shown in the left pane;
clicking one, causes all the todos in that view to be displayed on the main
pane.

Some default views are available:

- 📮 **Inbox**: tasks with no context
- 🚀 **Active**: tasks that
  - do not have `@@blocked`, `@@delegated` or `@@maybe` contexts
  - are before their threshold date
  - are not `blocked` (i.e., do not depend on another, undone task)
- 🚨 **Due Soon**: tasks due within 2 weeks
- ⏰ **Coming up**: tasks with thresholds within 2 weeks
- 🤝 **Delegated**: tasks with the `@@delegated` context
- 🛑 **Blocked**:
  - tasks with the `@@blocked` context
  - tasks that are `blocked`
- ✋ **Blocking**:
  - tasks that are blocking other tasks (are `dep:`s for them)
  - topologically sorted by "most dependencies" (warn if circular deps?)
- 🔮 **Future**: all tasks with thresholds in future
- 😴 **Inactive**: task with the `@@maybe` or `@@blocked` contexts
- 🕸️ **Stale**: task created long ago, that still aren't done (unless marked with `@sticky`)
- All?

#### 🛢️ `todo.sql`

Views are nothing but `.todo.sql` files placed
in`$HOME/.config/todo-hs/views/`. The folder is pre-populated with the
built-in views listed above.

Other, custom views can be added with
[`todo.sql`](@/1_rings/tudor/3_todo.sql.md). The names of the views, and the
order they appear in the left pane, is determined by the filename. For
example, if a view file is named something like `10-inbox.todo.sql`, then:

- the `10-` prefix will be used for ordering (unix style) and stripped from the display name; and
- the `.todo.sql` suffix will also be also stripped

So the resulting view would be named `inbox` and appear towards the top
(e.g., before a `20-` but after a `5-`).

### @Contexts and +Projects

> **`TODO`**:

- filter by context and project
  - by interacting with sidebar
  - order prjs/ctxs with by something clever
    - **most number of active tasks**
    - most recent?
      - Needs to analyse tail of done.txt for recently completed tasks?
      - ...and todo.txt for recently completed and recently added tasks
    - something else?
- normal and inverted, see shortcuts

## 🦉 Main pane

Todos are shown in a table.

- `context` headers (can be switched to `project` headers with a shortcut)
- 2 lines per todo

Here is a sample list with 3 todos:

```todotxt
⸺ @errands ⸻⸻⸻⸻⸻⸻⸻⸻⸻⸻

  (A) buy panettone @errands +xmas
      C:2020-12-20   T:2020-12-21   D:2020-12-25
  (C) buy batteries @errands
      C:2020-12-20

⸺ @home ⸻⸻⸻⸻⸻⸻⸻⸻⸻⸻⸻

x (C) make the pasta @home
      C:2020-12-20 T:2020-12-20

```

**`TODO:`**

- hide context from tasks if viewing tasks by context
- show only other contexts (not one headered)
- tasks with multiple contexts show up multiple times
- possible group bys:
  - project
  - context
  - priority
  - due date
  - threshold date
  - completion date

## ✨ Fuzzy-Search / New Task dialogue {#fuzzy-newtask}

> **`TODO:`**

Features:

- fzf-completion for `@contexts`, `+projects`, tags (e.g. `due:`)
  - support something like [`topydo`'s filter expression](https://cdn.rawgit.com/bram85/topydo/master/docs/index.html#FilterExpressions)
- auto adding of creation date
- auto adding of completion date if adding an already completed todo (e.g., for tracking)
- convert `t:` `due:` dates if entered in [relative/fuzzy mode](@/1_rings/tudor/1_cli.md#snoozing)

As you type, todos in the main view below are filtered. At any moment, you
may press `Ctrl-RET` to add a todo with the text you have entered (Similar to
[`deft`](https://jblevins.org/projects/deft/) or [Notational
Velocity](http://notational.net/)).

> **NOTE:** most features are shamelessly stolen from
> [`topydo`](https://cdn.rawgit.com/bram85/topydo/master/docs/index.html#add)

### :`Ex` mode

Starting the line with `:` enters `Ex` mode. Some [`tudor-cli`](@/1_rings/tudor/1_cli.md#usage) commands are supported. E.g.

```ruby
:pri a 1 2 38 # prioritize todos 1, 2, and 38 with (A)
:archive # archive
:snooze ~1w 3 4 # snooze todos 3, 4 by roughly 1 wee
```

## 📅 Dates

Dates are can be shown in [relative human
format](https://stackoverflow.com/questions/11/calculate-relative-time-in-c-sharp)
format instead of `YYYY-MM-DD`, this can be toggled globally with a shortcut.

## ⌨️ Keyboard shortcuts

### 🎯 Focusing shortcuts

Focusing shortcuts, to select activate a panel. Once a panel is activated,
its outline will turn orange (**`TODO:`**). These shortcuts are `Ctrl-` based:

| `Keys`   | `Action`               |
| -------- | ---------------------- |
| `Ctrl-f` | Focus Fuzzy Search bar |
| `Ctrl-v` | Focus Views panel      |
| `Ctrl-c` | Focus Contexts panel   |
| `Ctrl-p` | Focus Projects panel   |

### ️🔫 Action shortcuts

#### ↕️ Moving

| `Keys`         | `Action`                                    |
| -------------- | ------------------------------------------- |
| `j` or `↓`     | Move down to next view/todo/context/project |
| `k` or `↑`     | Move up to next view/todo/context/project   |
| `gg` or `home` | Move to first view/todo/context/project     |
| `G` or `end`   | Move to last view/todo/context/project      |

#### ⇵️ Sorting

Once you are focused on a panel, you can press `s` followed by another key to sort its contexts.

| `Keys` | `Action`                                        |
| ------ | ----------------------------------------------- |
| `s #`  | Sort by number of todos in view/context/project |
| `s n`  | Sort by name of todos in view/context/project   |
| `s x`  | Reset sorting to default                        |

> **TIP**: The default sorting is # of todos for Contexts and Projects,
> and the for Views

#### 🌓 Enabling/Disabling

The Contexts and Projects panels can be used to further narrow down the
current view. By default they will list all the contexts/projects for which a
task is present in the current view and how many tasks have that
context/project in that view.

Both panels can be in normal or inverted mode; in normal mode, the panel
headings will be `Contexts` and `Projects` respectively. In inverted mode,
the pane headings will be `Contexts [inverted]` and `Projects [inverted]`.

| `Keys` | `Action`                                           | Visual cue            |
| ------ | -------------------------------------------------- | --------------------- |
| `v`    | toggle between inclusive and exclusive filter mode | `inc`/`excl` in panel |
| `SPC`  | Toggles state of context/project                   | N/A                   |
| `x`    | Resets (enables all contexts)                      | N/A                   |

Move up or down with arrows or with `j` and `k`.

### 🌍 Global shortcuts

| `Keys`   | `Action`                                           |
| -------- | -------------------------------------------------- |
| `Meta-h` | Toggle between human and `YYYY-MM-DD` date formats |
| `Ctrl-x` | Reset all panes, go back to inbox                  |
| `Ctrl-q` | Quit                                               |

> **TIP:** a hint to quit with `Ctrl-Q` appears if `Ctrl-C` is pressed twice,
> since `Ctrl-C` is reserved for focusing the Contexts panel.

> **See also:** [`topydo` shortcuts](https://cdn.rawgit.com/bram85/topydo/master/docs/index.html#ColumnShortcuts)
