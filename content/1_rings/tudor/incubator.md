+++
title = "🧪 Incubator"
+++

Here we find:

- remarks that don't belong anywhere else
- features that still need thinking / polishing
- open issues

## [`tui`][`cli`] Dependencies between tasks

This is how we implement tasks that depend on each other. The idea of
"sequential projects" —where tasks must be executed in a strict order— is
also discussed here.

These operations should be done mainly through the `tui` as it would be a
PITA to do from the `cli`. The `cli`, however, will not show "blocked" tasks
(i.e., tasks that depend on incomplete ones) in certain scenarios.

Also, note that a single todo can have many `p:`s.

> **See also:** [Dependencies on `topydo`](https://cdn.rawgit.com/bram85/topydo/master/docs/index.html#Dependencies)

### [`tui`] Ad hoc

1. you press a key (`P`for "parent")
   - a fzf completion box appears
   - if current task has a project, tasks in the same project are listed first
1. you select the task that your task depends on and press enter
   - `id:{NANOID(6)}` is added to the dependency task if none is present
     (See [`nanoid`](https://zelark.github.io/nano-id-cc/))
   - `p:{NANOID_of_dependency}` is added to the current task

### [`tui`] "sequential" projects

- you select a project from the side/bar or wherever
- you mark it as "sequential"
- you can rearrange tasks with `Meta-J` and `Meta-K` or `Meta-UP` and `Meta-DOWN`

## Recurrent tasks

> **NOTE**: i'm not sure I will implement this as a calendar's recurring event
> functionality is enough for me

Recurrent tasks could work like in
[Simpletask](https://github.com/mpcjanssen/simpletask-android/blob/master/app/src/main/assets/index.en.md).

## Niceties

- support the [adding niceties of topydo](https://cdn.rawgit.com/bram85/topydo/master/docs/index.html#add)
- [`cli`] support stable IDs like in [`todo-hs`](https://gitlab.com/vise890/todo-hs)
- local/global todos like in `todo-hs`, but also with:
  - override `--global`
  - override `--file=`
  - local config as well (for views)
  - views in `$TODO_DIR/views`
  - specify `TODO_DIR` and views in `$HOME/.config/tudor/`?
