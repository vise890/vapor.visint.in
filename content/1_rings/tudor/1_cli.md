+++
title = "🕶️ CLI"
weight = 1
+++

Some `todo.txt` interactions are better served by the CLI. Here are some
examples:

## 🍱 Batch-adding heterogeneous todos

Adding multiple todos can be done with `t add`:

- the creation date is automatically added (set to today); and
- the completion date is automatically added when adding a completed todo
  (e.g., for tracking reasons).

Example usage:

```bash
$ t add '
@mom teach me gooey-core mini-chocolate-cake recipe
@phone call Bob
x @pc cleanup tudor-cli notes'
```

> **See also:** adding [new tasks in the TUI](@/1_rings/tudor/2_tui.md#fuzzy-newtask)

## 👁️ Quick glances

You can use `t ls` to look at the tasks that are available in a context, or
what's actionable next for a project:

For example:

- `t ls @foo` — list active tasks in context `@foo`
- `t ls @foo @bar` (`@foo` or `@bar`)
- `t ls +foo` — list active tasks in project `+foo`
- `t ls (A)` — list active tasks with priority `A`
- `t ls (A-C)` — list active tasks with priority `A`, `B`, or `C`
- `t contexts` or `t ctx` — list all contexts
- `t projects` or `t prj` — all projects
- `t grep` — grep through all active tasks
  - `t gr` or `t ag` (as in `ag`, the silver searcher? easier to type)
- `t ls {base_name_of_todo.sql_view}`
  - e.g. `t ls inbox`
  - see [todo.sql](@/1_rings/tudor/3_todo.sql.md)

By default, some todos are hidden:

- todos with a threshold date in the future;
- todos that are `@@delegated` or `@@blocked` (tasks that depend on undone tasks).

You can still display all todos via the `--all` flag.

In these views, the **following todo properties are hidden**:

- creation, completion, and threshold dates
- `id:`
- `h:`

The due date is shown only if it is close (< 1month) and it is colored in
green, amber or red according to how close it is.

## 😴 Snoozing {#snoozing}

You can snooze a todo (i.e., move its threshold date) by exact amounts, but
also by random-ish amounts so that the todos don't all reappear on the same
day if snoozing is done in a batch.

For example:

```shell
t snooze ~1w 1 2 4
```

... will set 3 different threshold dates for todo `1`, `2` and `4`.

The following snooze codes are available:

| Snooze code     | Human snooze code | Effect                       |
| --------------- | ----------------- | ---------------------------- |
| `x`             | None              | removes threshold date       |
| `1d`            | Tomorrow          | `1d` (i.e., tomorrow)        |
| `~2d` 🎲        | Two days          | `1-3d`                       |
| `~1w` 🎲        | About one week    | `1w +/- 1d`                  |
| `~2m` 🎲        | About two months  | `2m +/- 5d`                  |
| `~1y` 🎲        | About one year    | `1y +/- 1w`                  |
| `2020-10-11`    | Pick a date       | `2020-10-11`                 |
| `next thursday` | Pick a date       | `next thursday`              |
| `next week`     | Next week         | `next monday` (check locale) |
| `nw`            | Next week         | `next monday` (check locale) |
| `next month`    | Next month        | `beginning of next month`    |
| `nm`            | Next month        | `beginning of next month`    |

> **See also:** The [Napoleon technique](https://effectiviology.com/napoleon/) involves
> postponing tasks, when progress is likely to be made on them by someone else
> over time without your input.

## 🪄 Autocompletion

Autocompletion is supported for contexts, projects and views.

For example, typing:

```sh
t ls @b⇥
```

...offers a list of completions, such as:

```txt
@barbara
@beach
@bill
@barn
```

> **`TODO:`** see
> [`oh-my-zsh` autocompletion for `stack`](https://github.com/robbyrussell/oh-my-zsh/blob/master/plugins/stack/stack.plugin.zsh)
> for a good starting point for implementing zsh completions

## ⛑️ `--help` example {#usage}

```txt
tudor-cli - A todo.txt CLI manager

Usage: t COMMAND

Available options:
  -h,--help     Show this help text

Available commands:
  add (a)                Add todo(s)
  append (app)           Append TEXT to target todo(s)
  archive (ar)           Archive completed todos
  clean (cl)             Archive completed todos, sort and
                         remove whitespace in todo.txt
  complete (do)          Mark the target todo(s) as done
  edit (e)               Edit todo.txt file with $EDITOR
  grep (gr)              Search for text in todos
  list (ls)              List all todos/views/@contexts/+projects
  prepend (pre)          Prepend TEXT to target todo(s)
  prioritize (pri)       Give PRIORITY to target todo(s)
  remove (rm, del)       Delete target todo(s)
  snooze (postpone, snz) Snooze target todo(s) by the given
                         SNOOZE_AMOUNT
  undo                   Mark target todo(s) as not done (to do)
  uncomplete             Mark target todo(s) as not done (to do)
  unprioritize (unpri)   Remove priority from target todo(s)
  contexts (ctx, ctxs)   List all the contexts
  projects (pj, pjs)     List all the projects
  inbox (in)             List todos in the inbox (i.e. todos without a
                         context)
```

## 🎨 Prior art

- [todo-hs](https://gitlab.com/vise890/todo-hs) My own implementation of some
  of the features
- [`topydo`](https://cdn.rawgit.com/bram85/topydo/master/docs/index.html)
- [`todotxt-machine`](https://github.com/AnthonyDiGirolamo/todotxt-machine)
- The original [`todo-cli`](https://github.com/todotxt/todo.txt-cli)
