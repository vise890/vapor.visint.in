+++
title = "👑 tudor"
+++

Tudor is a suite of applications for dealing with the lightweight,
future-proof and outright awesome
[`todo.txt`](https://github.com/todotxt/todo.txt) ecosystem.

As a sneak peek, a `todo.txt` looks something like this:

```todo.txt
(A) enjoy the little things
book 0-g training @net +space-trip
x 1999-12-11 2018-08-25 become a laser-tag master @laser-dojo
```

This _ring_ contains:

- a [🕶️ `CLI`](@/1_rings/tudor/1_cli.md) — for quick interactions at the command line;
- a [🖼️ `TUI`(GUI?)](@/1_rings/tudor/2_tui.md) — for adding, manipulating and viewing tasks; and
- an implementation of [🛢️ `todo.sql`](@/1_rings/tudor/3_todo.sql.md) — a sql dialect to query `todo.txt` files.

There is also the [🧪 _incubator_](@/1_rings/tudor/incubator.md) for stuff
that is cross-cutting or idk where to put yet.

## 🎨 Prior art

- A [primer on `todo.txt`](https://github.com/todotxt/todo.txt)
- The original [`todo-cli`](https://github.com/todotxt/todo.txt-cli)
- [`topydo`](https://cdn.rawgit.com/bram85/topydo/master/docs/index.html)
- [`todotxt-machine`](https://github.com/AnthonyDiGirolamo/todotxt-machine)
- 🖼️ [QTodoTxt](https://github.com/QTodoTxt/QTodoTxt)
- my own [`todo-hs`](https://gitlab.com/vise890/todo-hs)
- [Omnifocus](https://www.omnigroup.com/omnifocus) — 💵, `mac-only`
