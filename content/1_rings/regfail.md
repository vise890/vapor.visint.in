+++
title = "❗ regfail"
+++

Explains to a human why some string won't match the provided regular
expression. Provides hints as to how to correct it, if possible.

It can be used to automatically generate validation errors for
RegEx-based validation rules.

Fully i18n-zed messages can be produced.

| Input     | Regular Expression | Message                                                     |
| --------- | ------------------ | ----------------------------------------------------------- |
| `h@xor99` | `[a-z0-9]`         | Only lowercase letters and digits are allowed. Replace `@`. |

> **`TODO:`** more examples

## 🎨 Prior art

- [regal](https://github.com/lambdaisland/regal)
