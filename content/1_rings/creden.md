+++
title = "↹ creden"
+++

A Conflict-Free Replicated [EDN](https://github.com/edn-format/edn) Data Type.

## 🎨 Prior art

- [JSON implementation by Martin Kleppmann](https://arxiv.org/abs/1608.03960)
