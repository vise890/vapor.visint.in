+++
title = "📖 mdfmt"
+++

Markdown vetter/formatter/prettifier. Messy markdown goes in,
[CommonMark](https://commonmark.org/) comes out.

## Features

- 80char lines (**in lists and quotations as well**, properly wrapped)
- right spacing between headers, code blocks
- normalized bullets (e.g., `1. 1. 1.` vs `1. 2. 4.`)
- possibly fixing some DWIMs in markdown and converting them to CommonMark?
  - or providing a stricter `migrate` command that would warn on suspicious/ambiguous
    cases and suggest the CommonMark alternative?

## 🎨 Prior art

- [Prettier](https://prettier.io/) already does a decent job at formatting
  - but it doesn't keep the content width constant at 80-chars tho
