+++
title = "Vapor"
description = "Tales of software that doesn't exist"
+++

## Tales of software that doesn't exist

This site is about software that I wish existed. It would be simple yet
powerful when composed, clean, free (as in freedom), and hopefully useful.

The whole thing is intended for dreamingly amusing myself; all of these ideas
are either too big or too difficult for me to build alone.

The site is divided in two sections:

- [💍 _rings_](@/1_rings/_index.md) — nearly formed concepts that could be implemented; and
- [☁️ _puffs_](@/2_puffs/_index.md) — not yet fully formed concepts (an incubator for _rings_).
