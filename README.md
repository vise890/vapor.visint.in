# Vapor

Tales of software that doesn't exist.

Deployed on:
- [vapor.visint.in](https://vapor.visint.in)
- [GitLab pages](https://vise890.gitlab.io/vapor/)

## Local build

```sh
git submodule update --init --recursive
zola build
```
